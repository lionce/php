FROM php:5-cli

COPY php.ini /usr/local/etc/php/php.ini

#Install memcached
RUN apt-get update && apt-get install -y libssl1.0.0 libssl-dev zlib1g zlib1g-dev libmemcached-dev \
    && pecl install memcached \
    && docker-php-ext-enable memcached

#Install mongodb
RUN pecl install mongodb
RUN docker-php-ext-enable mongodb

# Install bcmath
RUN docker-php-ext-install bcmath

# Install bz2
RUN apt-get install -y libbz2-dev
RUN docker-php-ext-install bz2

# Install calendar
RUN docker-php-ext-install calendar

# Install GD
RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng12-dev
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install gd

# Install mbstring
RUN docker-php-ext-install mbstring

# Install mcrypt
RUN apt-get install -y libmcrypt-dev
RUN docker-php-ext-install mcrypt

# Install PDO
RUN docker-php-ext-install pdo_mysql

# Install ftp
RUN docker-php-ext-install ftp

# Install intl
RUN apt-get install -y libicu-dev
RUN pecl install intl
RUN docker-php-ext-install intl

RUN mkdir -p app
VOLUME /app
WORKDIR /app

ENTRYPOINT ["php"]
